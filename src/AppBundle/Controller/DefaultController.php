<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Record;
use AppBundle\Form\CreateRecordForm;
use AppBundle\Form\UpdateRecordForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller {
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        // Record form build

        $repository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $repository->findAll();

        $task = new Record();
        $task->setDate(new \DateTime('today'));

        $form = $this->createFormBuilder($task)
            ->add('content', TextareaType::class, array('label' => false,
                'attr' => array('placeholder' => 'Vpište váš obsah záznamu.', 'maxlength' => '255', 'onkeyup' => 'countChar(this)')))
            ->add('date', DateType::class, array('label' => false))
            ->add('category', EntityType::class, [
                "label" => "Kategorie",
                'class' => 'AppBundle\Entity\Category',
                'placeholder' => 'Zvolte kategorii',
                'choice_label' => 'name',
            ])

            ->add('save', SubmitType::class, array('label' => 'Vložit'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();
        }

        $repository = $this->getDoctrine()->getRepository(Record::class);
        $records = $repository->findAll();

        $form2 = $this->createForm(CreateRecordForm::class, new Category(), ['action' => $this->generateUrl('createCategory')]);

        /** @var Record $record */
        foreach ($records as $record) {
            $record->form = $this->createForm(UpdateRecordForm::class, $record, [
                'action' => $this->generateUrl('update', ['id' => $record->getId()])
            ])->createView();
        }

        return $this->render('default/index.html.twig', array(
            'form' => $form->createView(),
            'form2' => $form2->createView(),
            'record' => $records,
            'categories' => $categories
        ));
    }


    /**
     * @Route("/createCategory", name="createCategory")
     */
    public function createAction(Request $request) {

        $form = $this->createForm(CreateRecordForm::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('homepage');
    }


    /**
     * @Route(
     *      "/{id}/delete",
     *      name="delete"
     * )
     */
    public function removeAction($id, Request $request) {
        $something = $this->getDoctrine()
            ->getRepository('AppBundle:Record')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($something);
        $em->flush();

        return $this->redirect($this->generateUrl('homepage'));
    }


    /**
     * @Route(
     *      "/{id}/update",
     *      name="update"
     * )
     */
    public function updateAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $record = $em->getRepository(Record::class)->find($id);

        if (!$record) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }

        $form = $this->createForm(UpdateRecordForm::class, $record);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('homepage');
    }
}