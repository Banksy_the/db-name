<?php

namespace AppBundle\Command;

use OldBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppDummyDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:dummyData')
            ->setDescription('...')
            ->addArgument('name', InputArgument::REQUIRED, 'Argument description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');

        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();


        $post = new Post();
        $post->setName($name);
        $em->persist($post);
        $em->flush();

        $output->writeln('Post created.');
    }

}
