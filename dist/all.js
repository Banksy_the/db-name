function countChar(val) {
    var len = val.value.length;
    $('#char-counter').text(len);
}

$(function () {
    $('.delete-button-toogle').click(function (event) {
        var toggleData = $(event.target).data('id-toggle');
        $('#toggle-'+ toggleData).show();
    });

    $('.toggle-delete-record').click(function(){
        $(this).hide();
    });
});

$(function () {
    $('form[action*="update"]').submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: this.getAttribute('action'),
            data: $(this).serialize()
        });
    })
});

$(function () {
    $('.delete-button').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: this.getAttribute('href'),
            success: this.closest('form').remove()
        });
    })
});

$(function () {
    $('#form-add').submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            data: $(this).serialize(),
            success: function (response) {
                $( 'body' ).html(response);
            }
        });
    })
});

/*
$(function () {
    $('#form-category-add').submit(function (e) {
        console.log('-t-' + $(this).serialize() + '-t-');
        e.preventDefault();
        $.ajax({
            type: "POST",
            data: $(this).serialize(),
            success: function (response) {
                $( 'body' ).html(response);
            }
        });
    })
});
*/

$(document).ajaxStart(function() {
    $('#loading-div').css('display', 'flex');
});

$(document).ajaxStop(function() {
    $('#loading-div').css('display', 'none');
});